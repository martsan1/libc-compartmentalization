#include <stdio.h>
#include <time.h>
#include <langinfo.h>
#include <locale.h>
#include <stdlib.h>

// #include <linux/kernel.h>
// #include <sys/syscall.h>
#include <unistd.h>

#define TEMPBUF_SIZE	100

int main()
{
    nl_catd catalog;
    time_t  timing;
    char    tempbuffer[TEMPBUF_SIZE];

    clock_t start_time;
    double elapsed_time;

	unsigned long result;

    system("cat /proc/$PPID/maps");

    result = 0;
    // result = syscall(333, "intlHello");

    setlocale (LC_ALL, "");

	start_time = clock();

    for(int i=0; i<2; i++)
    {
    	printf("%d)\n", i);

	    if ((catalog = catopen ("/home/amd/Desktop/sample.SJIS", 0)) == (nl_catd) -1)
	    {
			fprintf(stderr, "Can't open message catalog\n");
			/* Continue anyway, this is not fatal */
	    }

	    (void) time(&timing);
	    (void) strftime(tempbuffer, TEMPBUF_SIZE,
		           catgets(catalog, 1, 2, "%a %b %T %y"),
		           localtime(&timing)
	           );
	    
	    printf("%s %s\n",
			catgets(catalog, 1, 1, "Hello world (English)"),
			tempbuffer);

	    catclose(catalog);
	}

	elapsed_time = (double)(clock() - start_time) / CLOCKS_PER_SEC;
	printf("Elapsed time: %f\n", elapsed_time);
	
	printf("Result: %ld\n", result);

    exit(0);
}
