import os

path = "/home/amd/Desktop/glibc-2.32"

open(os.path.expanduser('~') + '/Desktop/glibc_count.txt', 'w').close()

total_dir_count = 0
total_file_count = 0
total_LoC_count = 0
with open(os.path.expanduser('~') + '/Desktop/glibc_count.txt', 'a+') as glibc_count:
    for dir_name, dirs, files in os.walk(path):
        # print dir_name
        total_dir_count += 1
        for fname in files:
            total_file_count += 1
            fpath = os.path.join(dir_name, fname)
            file = open(fpath, "r")
            line_count = 0
            for line in file:
                if line != "\n":
                    line_count += 1
                    total_LoC_count += 1
            file.close()
            # print fpath, line_count
            glibc_count.write(fpath + ', LoC: ' + str(line_count) + '\n')


    glibc_count.write('Total directories: ' + str(total_dir_count) + '\n')
    glibc_count.write('Total files: ' + str(total_file_count) + '\n')
    glibc_count.write('Total LoCs: ' + str(total_LoC_count) + '\n')
